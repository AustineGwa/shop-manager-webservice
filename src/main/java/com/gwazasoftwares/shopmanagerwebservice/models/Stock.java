package com.gwazasoftwares.shopmanagerwebservice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Stock {

    @Id
    private String productId;
    @Column
    private String productType;
    @Column
    private String productQuantity;
    @Column
    private int    productUnitPrice;
    @Column
    private int    productTotalPrice;

}
