package com.gwazasoftwares.shopmanagerwebservice.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class User {

    @Id
    private String userId;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String usertype;
}
