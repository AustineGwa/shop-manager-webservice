package com.gwazasoftwares.shopmanagerwebservice.Repositories;

import com.gwazasoftwares.shopmanagerwebservice.models.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by User on 9/25/2018.
 */
public interface StockRepository extends JpaRepository<Stock, String> {
}
