package com.gwazasoftwares.shopmanagerwebservice.Repositories;

import com.gwazasoftwares.shopmanagerwebservice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
