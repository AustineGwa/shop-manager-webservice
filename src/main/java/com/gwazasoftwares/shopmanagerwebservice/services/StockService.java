package com.gwazasoftwares.shopmanagerwebservice.services;

import com.gwazasoftwares.shopmanagerwebservice.Repositories.StockRepository;
import com.gwazasoftwares.shopmanagerwebservice.models.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Service
public class StockService {

    @Autowired
    StockRepository stockRepository;


    public List<Stock> getAllStock(){
        return  stockRepository.findAll();
    }

    public void addStock(Stock  stock){
        stockRepository.save(stock);
    }

    public void deleteStock(String stockId){
        stockRepository.deleteById(stockId);
    }


}
