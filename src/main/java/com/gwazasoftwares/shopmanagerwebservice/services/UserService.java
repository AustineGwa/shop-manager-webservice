package com.gwazasoftwares.shopmanagerwebservice.services;

import com.gwazasoftwares.shopmanagerwebservice.Repositories.UserRepository;
import com.gwazasoftwares.shopmanagerwebservice.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by User on 9/25/2018.
 */

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllUsers(){
        return  userRepository.findAll();
    }

    public void addUser(User user){

        userRepository.save(user);

    }

    public void deleteUserById(String userId){
        userRepository.deleteById(userId);
    }

    public User getUserById(String userId) {
       return userRepository.getOne(userId);
    }
}
