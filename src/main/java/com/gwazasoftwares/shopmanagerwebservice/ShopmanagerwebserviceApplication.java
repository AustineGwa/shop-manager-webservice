package com.gwazasoftwares.shopmanagerwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopmanagerwebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopmanagerwebserviceApplication.class, args);
	}
}
