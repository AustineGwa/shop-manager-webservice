package com.gwazasoftwares.shopmanagerwebservice.controllers;

import com.gwazasoftwares.shopmanagerwebservice.models.User;
import com.gwazasoftwares.shopmanagerwebservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/users/")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/all")
    public List<User> getAllUsers(){
       return  userService.getAllUsers();

    }

    @GetMapping("/{userId}")
    public User getUserById(@RequestParam String userId){
        return  userService.getUserById(userId);

    }

    @PostMapping
    public String addUser(@RequestBody User user){
        userService.addUser(user);
        return  "user added successfully ";

    }

    @PutMapping("/{userId}")
    public User editUser(@RequestParam String userId){
        User editedUser = userService.getUserById(userId);
        userService.addUser(editedUser);
        return  editedUser;

    }

    @DeleteMapping("/{userId}")
    public  String  deleteUser(@RequestParam String userId){
        userService.deleteUserById(userId);

        return "user deleted successfully  message";
    }



}
