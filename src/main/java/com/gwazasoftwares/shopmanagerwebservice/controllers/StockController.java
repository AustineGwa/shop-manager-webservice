package com.gwazasoftwares.shopmanagerwebservice.controllers;

import com.gwazasoftwares.shopmanagerwebservice.models.Stock;
import com.gwazasoftwares.shopmanagerwebservice.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/stock/")
public class StockController {

    @Autowired
    StockService stockService;

    @GetMapping("/all")
    public List<Stock>  getAllStock(){
       return  stockService.getAllStock();
    }

    @PostMapping
    public String addStock(@RequestBody Stock stock){
        stockService.addStock(stock);

        return  "stock  saved  successfully";
    }





}
